// ignore_for_file: unrelated_type_equality_checks

import 'dart:math';
import 'dart:io';

class Living {
  String name = '';
  String choice = '';

  String getName() {
    return name;
  }

  void setName(String name) {
    this.name = name;
  }

  String getChoice() {
    return choice;
  }

  void setChoice(String choice) {
    this.choice = choice;
  }
}

class Game {
  Living player = Living();
  Living computer = Living();
  Living choiceP = Living();
  Living choiceC = Living();
  bool win = false;
  int countP = 0;
  int countC = 0;
  int countAns = 0;
  var reset = 'n';

  void showWelcome() {
    print('Welcome to ROCK PAPER SCISSORS Game!');
    print('Please input your name');
  }

  void showPlayer() {
    String name = stdin.readLineSync()!;
    player.setName(name);
    print('Welcome to ROCK PAPER SCISSORS Game : $name !!');
  }

  void showChoice() {
    print('\n');
    print("Enter any one of the following inputs:  ");
    print("ROCK");
    print("PAPER");
    print("SCISSORS");
    getPlayerChoice();
    getComputerChoice();
  }

  void getPlayerChoice() {
    print('\n');
    print('Please input your answer');
    String choosechoice = stdin.readLineSync()!.toUpperCase();
    choiceP.setChoice(choosechoice);
    print('Player Choice is $choosechoice');
  }

  bool doContinue() {
    print("Play again? (y/n):");
    reset = stdin.readLineSync()!;
    if (reset == 'y') {
      return true;
    } else if (reset == 'n') {
      print('Good bye!');
      return false;
    }
    return true;
  }

  void getComputerChoice() {
    String computerMove = '';
    int input = Random().nextInt(3) + 1;
    switch (input) {
      case 0:
        computerMove = 'ROCK';
        break;
      case 1:
        computerMove = 'PAPER';
        break;
      case 2:
        computerMove = 'SCISSORS';
        break;
    }
    choiceC.setChoice(computerMove);
    print('Computer Choice is $computerMove');
  }

  void isChoice() {
    //ROCK
    if (choiceP.getChoice() == 'ROCK' && choiceC.getChoice() == 'ROCK') {
      print('Draw');
      print('Player Score : $countP');
      print('Computer Score : $countC');
    } else if (choiceP.getChoice() == 'ROCK' &&
        choiceC.getChoice() == 'PAPER') {
      print('${player.getName()} Lose!');
      countC++;
      print('Player Score : $countP');
      print('Computer Score : $countC');
    } else if (choiceP.getChoice() == 'ROCK' &&
        choiceC.getChoice() == 'SCISSORS') {
      print('${player.getName()} Win!');
      countP++;
      print('Player Score : $countP');
      print('Computer Score : $countC');
      //PAPER
    } else if (choiceP.getChoice() == 'PAPER' &&
        choiceC.getChoice() == 'ROCK') {
      print('${player.getName()} Win!');
      countP++;
      print('Player Score : $countP');
      print('Computer Score : $countC');
    } else if (choiceP.getChoice() == 'PAPER' &&
        choiceC.getChoice() == 'PAPER') {
      print('Draw');
      print('Player Score : $countP');
      print('Computer Score : $countC');
    } else if (choiceP.getChoice() == 'PAPER' &&
        choiceC.getChoice() == 'SCISSORS') {
      print('${player.getName()} Lose!');
      countC++;
      print('Player Score : $countP');
      print('Computer Score : $countC');

      //SCISSORS
    } else if (choiceP.getChoice() == 'SCISSORS' &&
        choiceC.getChoice() == 'ROCK') {
      print('${player.getName()} Lose!');
      countC++;
      print('Player Score : $countP');
      print('Computer Score : $countC');
    } else if (choiceP.getChoice() == 'SCISSORS' &&
        choiceC.getChoice() == 'PAPER') {
      print('${player.getName()} Win!');
      countP++;
      print('Player Score : $countP');
      print('Computer Score : $countC');
    } else if (choiceP.getChoice() == 'SCISSORS' &&
        choiceC.getChoice() == 'SCISSORS') {
      print('DRAW');
      print('Player Score : $countP');
      print('Computer Score : $countC');
    }
  }
  bool checkChoiceP() {
    countP = countP;
    if(countP==2) {
      return true;
    }
    return false;
  }
  bool checkChoiceC() {
    countC = countC;
    if(countC == 2) {
      return true;
    }
    return false;
  }
  bool isFinish() {
    if(checkChoiceC()) {
      return true;
    }else if(checkChoiceP()) {
      return true;
    }
    return false;
  }

  void showResult() {
     if (checkChoiceP()) {
      print('You Win!');
    } else if (checkChoiceC()) {
      print('You Lost!');
    }
  }
  void newGame() {
    countC = 0;
    countP = 0;
    print('\n');
    print("Enter any one of the following inputs:  ");
    print("ROCK");
    print("PAPER");
    print("SCISSORS");
    getPlayerChoice();
    getComputerChoice();
  }
}


void main(List<String> args) {
  Game game = Game();
  game.showWelcome();
  game.showPlayer();
  while (true) {
    game.showChoice();
    game.isChoice();
    if(game.isFinish()) {
      game.showResult();
      if(game.doContinue()) {
        game.newGame();
      }else {
        break;
      }
    }
  }
}
